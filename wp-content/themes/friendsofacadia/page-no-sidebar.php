<?php
/*
Template Name: Page with no sidebar
*/
?>
<?php get_header(); ?>

			<div id="content" class="interior">

				<div id="inner-content" class="wrap clearfix">
					<div id="page-content" class="twelvecol first last">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div id="mobile-section-menu">
                                Section Navigation
                            </div>
                            <div id="mobile-section-menu-items">
                                <?php include(locate_template('section-sub-nav.php')); ?>
                            </div>
                            <div class="breadcrumbs">
                                <?php if(function_exists('bcn_display')) {
                                    bcn_display();
                                } ?>
                                <div class="fb-like-wrapper"><?php echo do_shortcode( '[fb_button]' ); ?></div>
                            </div>

						    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    							<header class="article-header">

    								<div class="page-title-wrapper">
                                        <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
                                    </div>

    							</header> <!-- end article header -->

    							<section class="entry-content clearfix" itemprop="articleBody">
                                    <?php if ( has_post_thumbnail() ) : ?>
                                        <div class="featured-image">
                                            <?php the_post_thumbnail( 'full' ); ?>
                                        </div>
                                    <?php endif; ?>
    								<?php the_content(); ?>
                                </section> <!-- end article section -->

    						</article> <!-- end article -->

						<?php endwhile; ?>

                        <?php else : ?>

								<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
									</footer>
								</article>

						<?php endif; ?>
                        <?php //get_template_part( 'll-bean', 'logo' ); ?>
                        <?php //get_template_part( 'mobile', 'enews-signup' ); ?>
					</div>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>