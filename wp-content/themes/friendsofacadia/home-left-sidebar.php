<?php
    $left_sidebar_text = get_field('left_sidebar_text', 'option');
?>

<div class="content">
<?php
    echo $left_sidebar_text;

    
?>
</div>
<?php
	get_template_part('interior','sidebar');
 ?>

<div class="content">

	
    <div class="social-media">
		<div><a href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/footer/facebook.png" alt="Facebook" /></a></div>
		<div><a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/footer/twitter.png" alt="Twitter" /></a></div>
		<div><a href="<?php the_field('instagram_link', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/footer/instagram.png" alt="Instagram" /></a></div>

	</div>


</div>

