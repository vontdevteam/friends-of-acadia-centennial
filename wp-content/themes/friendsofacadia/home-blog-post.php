<?php
$blog_block_query = new WP_Query();

$args = array();
$args['post_type']      = 'post';
$args['orderby']        = 'date';
$args['order']          = 'desc';
$args['post_status']    = 'publish';
$args['posts_per_page'] = 1;

$blog_block_query->query($args);

?>
<section id="blog-listings" class="article-listing">
	<header class="home-section-title"><h2 class="cobblestones-header">Cobblestones</h2></header>
	<h4>The Friends of Acadia Blog</h4>
	<?php while ($blog_block_query->have_posts() ) : $blog_block_query->the_post(); ?>
		<article>
			<header><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3></header>
			<section class="excerpt">
				<?php the_excerpt(); ?>
			</section>
			<footer><?php the_date('F j, Y'); ?></footer>
		</article>
	<?php endwhile; ?>
	<div class="archive-link">
		<a href="<?php echo get_permalink( 875 ); ?>">see all posts
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/archive-arrow.png" alt="see all posts" /></a>
	</div>
</section>
