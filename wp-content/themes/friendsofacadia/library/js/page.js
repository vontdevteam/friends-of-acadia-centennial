
function setLeftSidebarHeight() {
	var sidebarHeight = jQuery('#left-sidebar').outerHeight();
	var contentHeight = jQuery('#page-content').outerHeight();

	/* getting viewport width */
    var responsive_viewport = jQuery(window).width();

    /* if is below 481px */
    if (responsive_viewport >= 768) {
    	if (sidebarHeight > contentHeight) {
			jQuery('#page-content').height(sidebarHeight);
		}
		else {
			jQuery('#left-sidebar').height(contentHeight);
		}
	}
}

jQuery(document).ready(function() {

	// Set a class on the selected subnav item
	jQuery('.current-menu-item a');

	jQuery(window).load(function() {
		//setLeftSidebarHeight();
	});
	jQuery(window).resize(function() {
		//setLeftSidebarHeight();
	});
});