							<?php
    							$sponsor_blurb = get_field('sponsor_blurb');
    							$sponsor_centennial_plans = get_field('sponsor_centennial_plans');
    							if (empty($sponsor_blurb)) :
    							    $sponsor_blurb = wp_trim_words($sponsor_centennial_plans, 50);
    							endif;
    				        ?>
							<article id="post-<?php the_ID(); ?>" <?php post_class( array('clearfix', 'sponsors-listing') ); ?> role="article">

								<header class="article-header">

									<h3 class="search-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>



								</header> <!-- end article header -->

								<section class="entry-content">

    								<?php if ( has_post_thumbnail() ) : ?>

    								<div class="logo">
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'medium' ); ?></a>
    								</div>

    								<?php endif; ?>

									<?php
									    $fields = get_fields();
									    //dump($fields);
									    echo $sponsor_blurb;
								    ?>
									<a href="<?php the_permalink(); ?>">Read more &gt;</a>
								</section> <!-- end article section -->

								<footer class="article-footer">

								</footer> <!-- end article footer -->

							</article> <!-- end article -->