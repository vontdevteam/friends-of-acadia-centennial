<?php

?>

<div class="event details">
		<div class="detail">


				<?php if (has_post_thumbnail()) :
						 the_post_thumbnail( 'full' ); endif;
				?>

				<br>
				<strong>
					<?php  if(get_field('event_start_date')): ?>
						Date: <?php $date= new DateTime(get_field('event_start_date')); echo $date->format('l M d, Y'); ?>

						<?php if(get_field('event_end_date')): ?>
						- <?php $date= new DateTime(get_field('event_end_date')); echo $date->format('l M d, Y'); ?>
					<?php endif; endif; ?>
				</strong>
				<br>
				<strong>
					<?php if(get_field('event_start_time')): ?>

						Time: <?php the_field('event_start_time'); ?>

						<?php if(get_field('event_start_time')): ?>
							-   <?php the_field('event_end_time'); ?>
					<?php endif; endif; ?>
				</strong>

				<p>
				<?php if(get_field('event_description')): ?>

					<strong> Description: </strong> <?php the_field('event_description'); ?>

				<?php endif; ?>

				</p>

				<?php if(get_field('event_location')): ?>
					<strong>Location: </strong> <?php the_field('event_location'); ?>
				<?php endif;?>

				<br>
				<?php if(get_field('event_contact_name')): ?>
					<strong>Contact Name: </strong> <?php the_field('event_contact_name'); ?>
				<?php endif;?>
				<br>

				<?php if(get_field('event_contact_name')): ?>
					<strong>Contact Phone #: </strong> <?php the_field('event_contact_phone_number'); ?>
				<?php endif;?>
				<br>

				<?php if(get_field('event_web_page')): ?>
					<strong>Event web page:  </strong> <a target="_blank" href="<?php the_field('event_web_page');  ?>"><?php the_field('event_web_page'); ?></a><br>

				<?php endif; ?>

				<?php if(get_field('hidden_sponsor')): ?>
					<strong>Event Sponsor: </strong>
					<?php $sponsor_id = get_field('hidden_sponsor', $post->ID);
						//echo "THE ID:", print_r($sponsor_id);

						$sponsor_data = new WP_Query();
						$args['post_type'] = 'sponsors';
						$args['p'] = $sponsor_id[0];
						$args['status'] ='publish';

						$sponsor_data->query($args);

					?>

					<?php if($sponsor_data->have_posts()): while($sponsor_data->have_posts()): $sponsor_data->the_post(); ?>

						<a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>

					<?php endwhile; endif;
						wp_reset_postdata();

						//echo "<pre> <br> Object:", print_r($sponsor_data), "</pre>";
					?>
				<?php endif; ?>
				<br />

				<?php if(get_field('this_is_a_free_event')): ?>
					<br />This is a free event.<br>
				<?php endif; ?>

				<p>

			<a href="/events/"> See all events  </a>
			</p>
		</div>
</div>