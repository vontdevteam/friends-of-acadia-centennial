<?php

    // get random product from random sponsor
	$foac_product = new WP_Query();
	$args = array();
	$args['post_type']      = 'merchandise';
	$args['orderby']        = 'rand';
	$args['post_status']    = 'publish';
	$args['posts_per_page'] = 1;
	$foac_product->query($args);
	/*
	$count = $foac_product->found_posts;

	$random = rand(1, $count);*/

?>
<style type="text/css">
	.foac-home-random-product{margin-bottom: 10px;}
	.foac-home-random-product .title{font-size: 1.35em;}
</style>
<section class="article-listing">
	<header class="home-section-title"><h2>Merchandise</h2></header>

    <article>

	<div class="foac-home-random-product">
		<?php while($foac_product->have_posts()): $foac_product->the_post(); ?>
				<a href="<?php the_permalink(); ?>">
				<?php
				/*$partner = get_field('product_partner', $post->ID);
				$partner_id = $partner[1]['ID'];*/

				if (has_post_thumbnail()) :
				 the_post_thumbnail( 'medium' );
	            endif;
            ?>

            <h3 class="title"> <?php the_title(); ?></h3></a>

        <?php  endwhile;?>
    	</div>
    </article>
	<div class="archive-link">
		<a class="arrow" href="/centennial-merchandise/">See all products</a>
	</div>
</section>