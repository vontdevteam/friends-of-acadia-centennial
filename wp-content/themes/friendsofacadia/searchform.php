<?php
/**
 * The template for displaying search forms in the Friends of Acadia theme
 *
 * @package WordPress
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="text" class="field" name="s" id="s" value="Search" />
		<input type="submit" class="submit" name="submit" id="searchsubmit" value="Go" />
	</form>
