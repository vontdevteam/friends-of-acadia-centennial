<tr>
	<td><?php

	if ( get_field('ongoing_event') ) :
		echo 'Ongoing';
	else :
		$start_date = get_field('start_date');

		if ( get_field('end_date') ) {
			$end_date = get_field('end_date');

			// Break out the date pieces
			$start_date_pieces = explode('-', $start_date);
			$end_date_pieces = explode('-', $end_date);

			if ( $start_date_pieces[0] != $end_date_pieces[0] ) {
				$date = new DateTime( $start_date );
				echo $date->format('F j, Y') . ' - ';
				$date = new DateTime( $end_date );
				echo $date->format('F j, Y');
			}
			elseif ( $start_date_pieces[1] != $end_date_pieces[1] ) {
				$date = new DateTime( $start_date );
				$date_end = new DateTime( $end_date );
				echo $date->format('F j') . ' - ';
				$date = new DateTime( $end_date );
				echo $date->format('F j, Y');
			}
			else {
				$date = new DateTime( $start_date );
				$date_end = new DateTime( $end_date );
				echo $date->format('F j') . ' - ';
				$date = new DateTime( $end_date );
				echo $date->format('j, Y');
			}
		}
		else {
			$date = new DateTime( $start_date );
			echo $date->format('F j, Y');
		}
	endif; ?></td>
	<td><?php

	$target = null;

	// Figure out the link
	if ( get_field('external_link') ) :
		$link = get_field('external_link');
		$target = "_blank";
	elseif ( get_field('page_link') ) :
		$link = get_field('page_link');
	else :
		$link = get_permalink();
	endif;

	?><a href="<?php echo $link ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"<?php if (!empty($target)) echo ' target="' . $target . '"'; ?>><?php the_title(); ?></a></td>
</tr>