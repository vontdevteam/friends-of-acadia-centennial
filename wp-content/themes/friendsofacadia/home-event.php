<?php
$events_block_query = new WP_Query();

$today = date('Y-m-d');

$args = array();
$args['post_type']      = 'events';
$args['meta_key']       = 'event_start_date';
$args['orderby']        = 'event_start_date';
$args['order']          = 'ASC';
$args['post_status']    = 'publish';

$args['meta_query'][] = array(
	'key'			=> "event_start_date",
	'value'			=> $today,
	'compare'		=> '>='
	);
$args['meta_query'][] = array(
	'key'       => 'show_on_home_page',
	'value'     => true,
	'compare'   => '='
);
$args['posts_per_page'] = 3;

$events_block_query->query($args);

?>
<style type="text/css">
	.foac-home-events .list{
  		margin-bottom: 10px!important;
	}
	.foac-home-events .list .event-title{
	  font-size: 1.35em!important;
	  line-height: 1.2em;
	}
	.foac-home-events .list .item{
		margin-bottom: 10px;
	}

</style>
<section class="article-listing foac-home-events">
	<!--<header class="home-section-title"><h2>Calendar</h2></header> -->
	<div class="list">
	<?php
	$i = 0; // Counter to show photo
	while ($events_block_query->have_posts() ) : $events_block_query->the_post(); ?>
			<div class="item">
			<?php $date = new DateTime(get_field('event_start_date', $post->ID));  ?>
			<?php /*if ( $i == 1 ) : ?>
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
			<?php endif; */ ?>

			<h3 class="event-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></h3>
				<?php echo $date->format('M d, Y');?><br>
				<?php //the_excerpt(); ?>
			</div>
	<?php $i = $i + 1;

	endwhile; 

	?>
	</div>
	<div class="archive-link">
		<a class="arrow" href="/get-involved/events/"  title="See all events">See all events</a>
	</div>
</section>