<?php
/*
* Template Name: display Events
*/

get_header(); ?>
			<div id="content" class="interior">
				<div id="inner-content" class="wrap clearfix">

					<div id="left-sidebar" class="threecol first">
						<?php

						if ( $post->post_type == 'news' ) :
							$page_id_for_sub_nav = 4706;
						elseif ( $post->post_type == 'events' ) :
							$page_id_for_sub_nav = 118;
						elseif ( $post->post_type == 'post' ) :
							$page_id_for_sub_nav = 875;
						endif;

						// Display the navigation for this section
						include(locate_template('section-sub-nav.php'));
						get_template_part( 'interior', 'sidebar' );

						?>
					</div>

						<div id="page-content" class="ninecol last">
							
							<div class="breadcrumbs">
                                <?php if(function_exists('bcn_display')) {
                                    bcn_display();
                                } ?>
                                <div class="fb-like-wrapper"><?php echo do_shortcode( '[fb_button]' ); ?></div>
                            </div>

                            <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    							<header class="article-header">

    								<div class="page-title-wrapper">
                                        <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
                                    </div>

    							</header> <!-- end article header -->
								
    						<section class="entry-content clearfix" itemprop="articleBody">

							<div class="fp-calendar">					
								<h3>
										<?php if(isset($_GET["switch_to_month"])){
											$act_month = date_create(preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"])."-01");
											$act_month = $act_month->format('Y-m');
 										}elseif(isset($_GET["date"])){
	 										$act_month = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["date"]);
 										}else{
											$act_month = date("Y-m");
										}  
										?>
										<a class="prev-month-link" href="/our-events/?switch_to_month=<?php echo date("Y-m",strtotime($act_month." -1 month")); ?>">&laquo; Previous</a>
										<?php 
											if(isset($_GET["switch_to_month"])){
												echo date("F",strtotime(preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"])."-01"));
											}else{
												echo date("F"); 
											}
										?>
										<a class="next-month-link arrow" href="/our-events/?switch_to_month=<?php echo date("Y-m",strtotime($act_month." +1 month")); ?>" >Next </a>
								</h3>
								<?php
								if(isset($_GET["switch_to_month"])){
									$new_month = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"]);
									$month_start =  str_replace("-", "" , $new_month."01") ;
									$end_of_month = str_replace("-", "" , $new_month."31") ;
								}else{								
									$month_start = date("Ym01");
									$today = date("Ymd");
									$end_of_month = date("Ymt");
								}
								?>
								<?php
								
								//get all events this month
								
								$metaquery = array(
								 array( //checking start date
								'key' => 'event_start_date',
								'value' => $month_start,
								'compare' => '>='
								  ),
								  array( //checking until end of month
								'key' => 'event_start_date',
								'value' => $end_of_month,
								'compare' => '<='
								  ),
								);
								
								$update_options = array(
								'posts_per_page'   => -1,
								'orderby'          => 'event_start_date',
								'order'            => 'ASC',
								'meta_query'      => $metaquery,
								'post_type'        => 'events',
								'post_status'      => 'publish'
								);
								
								
								global $wp_query;
								$events = query_posts($update_options);
								//get all days with an event in an array
								$eventdays = array();
								for ($x=0;$x<count($events);$x++){
									$date = $events[$x]->event_date;
									array_push($eventdays, substr($date,6,2));
								}//endfor
								
								$today = date("d"); //today's Date
								//find 1st of Month
								$month_start = strtotime($month_start);
								$firstweekday=date('w', $month_start);
								$day = 0-$firstweekday+1;
								echo "<table>";
								$month = substr($end_of_month, 4,2);
								$year = substr($end_of_month, 0,4);
								$days_in_month = cal_days_in_month(CAL_GREGORIAN,$month,$year);
								
								for($i=0;$i<6;$i++){
									echo "<tr>";
										for ($j=0;$j<7;$j++){
											if (!isset($_GET["switch_to_month"]) && $day == $today) echo "<td class='today' >";
											elseif (in_array($day,$eventdays)) echo "<td class='event'><a href='/our-events/?date=".$year.$month.$day."'>";
											elseif(!($day > 0 && $day <= $days_in_month)){
												echo "<td class='no-date' >";
											}else echo "<td>";
											if($day > 0 && $day <= $days_in_month) echo $day;
												$day++;
											if ($day == $today) echo "</td>";
											elseif (in_array($day,$eventdays)) echo "</a></td>";
											else echo "</td>";
										}
									echo "</tr>";
									if ($day > $days_in_month) break;
								}
								
								
								echo"</table>";
								echo"</div>";//close calendar widget  div
								wp_reset_query(); ?>
							<?php //dispaly counties ?>
							<p class="county-selection">Click below to see events for a particular county</p>
							<?php 
								$county_options = array(
									'posts_per_page'  => -1,
									'orderby'         => 'post_title',
									'order'           => 'ASC',
									'post_type'       => 'page',
									'child_of'	    => 69,
									'post_status'     => 'publish',
								);
								
								$counties = get_pages($county_options);
								echo "<ul class='county-list'>";
								foreach($counties as $county){
									echo "<li class='county'><a href='/our-events/?event_county=".$county->ID."' >";
										echo get_the_title($county->ID);
									echo "</a></li>";
								}//end foreach
								echo "</ul>";

							?>
						</div>
				<div id="event-list">
					<h1><?php the_title();  ?></h1>
					<?php 
						if(!isset($_GET["switch_to_month"])){
							$today = date("Ymd");
							$event_metaquery = array(array( 'key' => 'event_date','value' => $today,'compare' => '>='));
						}else{
							$new_month = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"]);
							$month_start =  str_replace("-", "" , $new_month."01") ;
							$end_of_month = str_replace("-", "" , $new_month."31") ;
							$event_metaquery = array(
								 array( //checking start date
								'key' => 'event_start_date',
								'value' => $month_start,
								'compare' => '>='
								  ),
								  array( //checking until end of month
								'key' => 'event_start_date',
								'value' => $end_of_month,
								'compare' => '<='
								  ),
								);
							
						}
						if(isset($_GET["event_county"])){
							$county = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['event_county']); //Sanitize $_GET
							$event_metaquery[] = array( 'key' => 'event_county','value' => $county,'compare' => 'LIKE');
						}
						

						if(isset($_GET["date"])){
							$single_date = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["date"]);
							if(strlen($single_date) == 7){//handke single digit dates, convert them to 0Day
								$single_date = substr($single_date, 0, 6)."0".substr($single_date, 6);
							}//end if 
							unset($event_metaquery);
							$event_metaquery[] = array( 'key' => 'event_date','value' => $single_date,'compare' => 'LIKE');
						}
						
						if(!isset($_GET["show_all_events"]) && !isset($_GET["date"]) && !isset($_GET["switch_to_month"])){
							$two_months = date("Ymd",strtotime("today + 2 months"));
							$event_metaquery[] = array( 'key' => 'event_start_date','value' => $two_months,'compare' => '<');
						}
						
						
						$events_options = array(
							'posts_per_page'  => -1,
							'orderby'         => 'meta_value',
							'meta_key'		  => 'event_start_date',
							'order'           => 'ASC',
							'post_type'       => 'events',
							'post_status'     => 'publish',
							'meta_query'      => $event_metaquery,
						);
						$events = new WP_Query($events_options);
						$current_month = 0;
						$x=0;
						if ($events->have_posts()) : while ($events->have_posts()) : $events->the_post();
							$date = get_field('event_start_date',$post->ID);
							$year = substr($date,0,4);
							$month = substr($date,4,2);
							$day = substr($date,6,2);
							
							if ($current_month == 0 || $current_month != $month){
								$x++;
								$current_month = $month;
								$act_year = date("y");
								$month_name = date("F", mktime(0, 0, 0, $month, $act_year));
								echo "<h3 class='month month-".$x."'><a name='".$month_name."'>".$month_name." Events</a></h3>";
							}
							$date = $month."/".$day."/".substr($year,2);
						    $event_link = get_permalink($post->ID);
						   ?>
						   <div class="event cf">
							  <h2><?php echo get_the_title($post->ID) ?></h2>
							  <p class='date-time-venue'><span class="title">Date:</span> <?php echo date("F j, Y", strtotime($date)); ?></p>
							  <?php 
								  if(get_field('event_time',$post->ID)){
							  			echo "<p class='date-time-venue'><span class='title'>Time:</span> ".get_field('event_time',$post->ID)."</p>";
								  } 
								  if(get_field('event_location',$post->ID)){
							  			echo "<p class='date-time-venue'><span class='title'>Venue:</span> ".get_field('event_location',$post->ID)."</p>";
								  } ?>
								<div class="event-image">
									<?php 
									if(get_field("event_image",$post->ID)){
										$image_temp = get_field("event_image",$post->ID);
										echo "<img src='".$image_temp["url"]."' alt='Event Image' />"; 
									}
									?>
								</div>
							  	<p><?php echo get_the_content($post->ID) ?></p>
						   </div>
					<?php endwhile; else : ?>
	
							<article id="post-not-found" class="hentry cf">
								<header class="article-header">
									<h2><?php echo ( 'No Event found') ; 
										if(isset($_GET["switch_to_month"])){
											echo " for ".date("F, Y", strtotime(preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"])));
										} ?>
									</h2>
								</header>
								<section class="entry-content">
									<p><?php _e( 'There is no event scheduled for your selection', 'bonestheme' ); ?></p>
								</section>
							</article>
	
					<?php endif; ?>
					<div class="all-events-link">
						<a class="read-more-link" href="/calendar/?show_all_events=true">List all Calendar Events > </a>
					</div>
					
				</div> <!-- end event list -->

				</section>
				</article>
				</div>
				</div>
		</div>

<?php get_footer(); ?>
