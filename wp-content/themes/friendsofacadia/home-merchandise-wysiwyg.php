<?php
    $middle_column_header = get_field('middle_column_header', 'option');
    $middle_column_text = get_field('middle_column_text', 'option');
?>

<section class="article-listing">
	<!--<header class="home-section-title"><h2><?php echo $middle_column_header ?></h2></header>-->
		<?php get_template_part( 'home', 'merchandise' ); ?>
		<article>
			<?php echo $middle_column_text ?>
		</article>

</section>