<div class="donate-button">
	<a href="<?php the_field('donation_form', 'option'); ?>" target="_blank">Donate<span class="smaller">To Acadia’s Centennial</span></a>
</div>

<!--
<?php get_template_part( 'sidebar', 'journal' ); ?>

<div class="button-wrap">
    <div class="enews-signup">
        <a href="<?php the_field('e_news_signup', 'option'); ?>" target="_blank">Sign Up for Our e-News</a>
    </div>
</div>
-->