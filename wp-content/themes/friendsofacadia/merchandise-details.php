<?php

?>

<div class="merchandise details">
		<div class="detail">
			 <h3> Product Information: </h3>



				<?php if (has_post_thumbnail()) :

						 the_post_thumbnail( 'full' );

			     endif; ?>

				<?php if(get_field('product_description')): ?>
					<p>
						<strong> Description: </strong>
						<?php the_field('product_description'); ?>
					</p>
				<?php endif; ?>

				<?php if(get_field('product_link')): ?>
				<p>
					<strong>Product web page:  </strong> <a target="_blank" href="<?php the_field('product_link');  ?>"><?php the_field('product_link'); ?></a>
				</p>
				<?php endif;?>



				<?php if(get_field('product_partner')): ?>
				<p>
					<strong>Offered to you by this Acadia Centennial Partner: </strong>
					<?php $sponsor_id = get_field('product_partner');
						//echo "THE ID:", print_r($sponsor_id);
						//echo "ECHO: ", $sponsor_id;

						$sponsor_data = new WP_Query();
						$args['post_type'] = 'sponsors';
						$args['p'] = $sponsor_id[0];
						$args['status'] ='publish';

						$sponsor_data->query($args);

					?>

					<?php if($sponsor_data->have_posts()): while($sponsor_data->have_posts()): $sponsor_data->the_post(); ?>

						<a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>

					<?php endwhile; else: echo "There are no products."; endif;
						wp_reset_postdata();

						//echo "<pre> <br> Object:", print_r($sponsor_data), "</pre>";
                    ?>
				</p>
					<?php endif;?>

                    <br />
                    <div><a href="/centennial-merchandise/">See all products</a></div>
		</div>
</div>