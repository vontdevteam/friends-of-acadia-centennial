<?php 
	


	$is_search_result = false;

	$foac_product_search = new WP_Query();
	$args = array();
	$args['post_type']      = 'merchandise';
	$args['orderby']        = 'rand';
	$args['post_status']    = 'publish';
	

	//echo "<pre>", print_r($foac_product_search),"</pre>";


	if(!empty($_POST)){

		$keyword = (!empty($_POST['bykeyword'])) ? true : false;

		if($keyword){
			$kw = trim($_POST['bykeyword'], " ");

			$foac_wp_s = $foac_product_search->query('s='.trim($kw, " "));	

			$args['meta_query'] = 
	 				array(
	 					'relation' => 'OR',
	 						array(
	 							'key' => 'product_description',
								'value' => trim($kw, " "),
	 							'compare' => 'LIKE',
	 						)
	 					);

		 		$foac_wp_customer = $foac_product_search->query($args);

		 		$foac_merch_search_result = array_merge($foac_wp_customer, $foac_wp_s);

		 		$post_ids = array();

				

					foreach($foac_merch_search_result as $res){
		 			$post_ids[] = $res->ID;

		 		}

		 		
		 		if(count($post_ids)){
		 			$rargs['post__in'] = $post_ids;

		 		}

		 		
		}

					if($_POST['bycategory'] !="all"){


						$rargs['tax_query'] = array(
									array(
										'taxonomy' => 'merchandise_categories',
										'field'	=>'slug',
										'terms' => $_POST['bycategory']
									)
							);
					}
				


					if(count($rargs)){

						$rargs['post_type'] = 'merchandise';
			 			$rargs['orderby']   = 'rand';
						$rargs['post_status']   = 'publish';
					}
		 			
		 

				//var_dump($rargs);
				//print_r($_POST);

				

	 			$results =  new WP_Query($rargs);

	 			$is_search_result = true;

	 			//print_r($results);
	}


?>


<div class="foac-merch-search clearfix">
	<form method="post" action="">
		<div class="input-group">
			<label for="bykeyword"> Search Merchandise for: </label>
			<input type="text" 
				<?php if(isset($_POST['bykeyword'])):?>
					value="<?php echo $_POST['bykeyword']; ?>";
				<?php endif; ?>
			id="bykeyword" name="bykeyword">
		</div>
		<div class="input-group">
			<label for="bycategory"> Category: </label>
				<?php

				//get list of Marchanside category
				$foac_merch_cat_list = get_terms('merchandise_categories');

				 //print_r(get_terms('merchandise_categories'));
			?>

			<select name="bycategory" id="bycategory">
				<option value="all"> All Categories</option>
				 <?php 
				 	 foreach($foac_merch_cat_list as $cat): ?>

				 	 <option  value="<?php echo $cat->slug;?>"><?php echo $cat->name; ?></option>
					
					<?php

				 	 endforeach;
				 ?>
			</select>

			
			
		</div>
		<div class="input-group submit">
			<input type="submit" value="Search" class="btn-foac-merch-seach">
		</div>
	</form>
	
</div>

<?php if($is_search_result):  ?>


	<div class="foac-merch-list">
		
		<?php if($results->have_posts()): ?>	

		<?php while($results->have_posts()): $results->the_post(); ?>
				<div class="item">
					<a href="<?php the_permalink(); ?>">
					<?php 
						$partner = get_field('product_partner', $post->ID);
						$partner_id = $partner[1]['ID'];
						
						if (has_post_thumbnail()) :
						 the_post_thumbnail( 'medium' );
			            endif;
	            	?>
            			<?php the_title(); ?></a>
            	</div>

        <?php  endwhile;?>
		<?php else:  ?>

		<div class="no-merch-search results">	<h2> Sorry, there are no products that match your search. </h2> </div>
		
		<?php endif; ?>
	
	</div>

<?php else:    $foac_product_search->query($args); ?>

	<?php //<pre> print_r($foac_product_search); </pre>?>

<div class="foac-merch-list">
		
		<?php while($foac_product_search->have_posts()): $foac_product_search->the_post(); ?>
				<div class="item">
					<a href="<?php the_permalink(); ?>">
					<?php 
						$partner = get_field('product_partner', $post->ID);
						$partner_id = $partner[1]['ID'];
						
						if (has_post_thumbnail()) :
						 the_post_thumbnail( 'medium' );
			            endif;
	            	?>
            			<?php the_title(); ?></a>
            	</div>

        <?php  endwhile;?>


</div>

<?php endif; ?>

