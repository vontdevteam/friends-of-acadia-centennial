<?php get_header(); ?>

			<div id="content" class="interior">

				<div id="inner-content" class="wrap clearfix">
					<div id="left-sidebar" class="threecol first">
						<?php

						if ( $post->ID == 1610 || $post->ID == 1581 || $post->ID == 1615 ) :
							$page_id_for_sub_nav = 118;
						else :
							$page_id_for_sub_nav = $post->ID;
						endif;

						// Display the navigation for this section
						include(locate_template('section-sub-nav.php'));
						get_template_part( 'interior', 'sidebar' );

						?>
					</div>
					<div id="page-content" class="ninecol last">
						<div id="mobile-section-menu">
                            Section Navigation
                        </div>
                        <div id="mobile-section-menu-items">
                        	<?php include(locate_template('section-sub-nav.php')); ?>
                        </div>
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        	<div class="breadcrumbs">
                                <?php if(function_exists('bcn_display')) {
                                    bcn_display();
                                } ?>
                                <div class="fb-like-wrapper"><?php echo do_shortcode( '[fb_button]' ); ?></div>
                            </div>

						    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    							<header class="article-header">

    								<div class="page-title-wrapper">
                                        <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
                                    </div>

    							</header> <!-- end article header -->

    							<section class="entry-content clearfix" itemprop="articleBody">
                                    <?php if ( has_post_thumbnail() ) :
                                        friendsofacadia_display_featured_image();
                                    endif; ?>
    								<?php the_content(); ?>

                                </section> <!-- end article section -->

    						</article> <!-- end article -->

						<?php endwhile; ?>
                        <?php else : ?>

								<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
									</footer>
								</article>

						<?php endif; ?>
						<?php
						if ( 317 == $post->ID ) :
							//get_template_part( 'll-bean', 'logo' );
						endif;

						?>
						<?php get_template_part( 'mobile', 'enews-signup' ); ?>
					</div>

				</div> <!-- end #inner-content -->
			</div> <!-- end #content -->

<?php get_footer(); ?>