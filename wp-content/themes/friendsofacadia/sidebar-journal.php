<?php /*
<div class="foa-journal">
	<img src="<?php echo get_template_directory_uri(); ?>/library/images/sidebar/journal-cover.png" />
	<div class="blurb">
		Receive our seasonal <strong>Friends of Acadia Journal</strong> as a benefit of membership
	</div>
</div>*/ ?>
<?php

$foa_cover = get_field('friends_of_acadia_journal_cover', 'option');

if ( !empty($foa_cover['url']) ) : ?>
	<div id="foa_sidebar" style="height: <?php echo $foa_cover['height']; ?>px;">
		<img src="<?php echo $foa_cover['url']; ?>" alt="<?php echo $foa_cover['alt']; ?>" class="journal-cover" />
		<div class="foa_sidebar_text">
			<?php echo get_field('friends_of_acadia_journal_text', 'option'); ?>
		</div>
	</div>
<?php endif; ?>
<div class="foa-journal-button">
	<a href="<?php echo get_permalink(340); ?>">Friends of Acadia Journal</a>
</div>