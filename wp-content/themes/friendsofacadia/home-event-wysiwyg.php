<?php
    $left_column_header = get_field('left_column_header', 'option');
    $left_column_text = get_field('left_column_text', 'option');
?>

<section class="article-listing">
	<header class="home-section-title"><h2><?php echo $left_column_header ?></h2></header>

		<article>
			<?php get_template_part('home','event'); ?>
			<?php echo $left_column_text ?>
		</article>

</section>