<?php

$sponsors_block_query = new WP_Query();

$args = array();
$args['post_type']      = 'sponsors';
$args['orderby']        = 'menu_order';
$args['order']          = 'ASC';
$args['post_status']    = 'publish';
$args['posts_per_page'] = 10;

$args['meta_query'] =array(
						array(
							'key'         => 'sponsor_type',
							'value'       => 'signature')
				);


$sponsors_block_query->query( $args );

$sponsors_text = get_field('sponsors_text', 'option');

?>
<div id="sponsors-and-partners" class="clearfix">
    <div class="wrap">

        <div class="twelivecol  thank-you">
            <h3>Thank you to our generous Acadia Centennial Signature Sponsors <a class="arrow" href="/centennial-partners/acadia-centennial-signature-corporations">Learn more</a></h3>
            
        </div>

        <div class="twelivecol logos">

            <?php while ($sponsors_block_query->have_posts() ) : $sponsors_block_query->the_post(); ?>
        		<div>
        			<?php


                    if ( get_field('sponsor_link') ) echo '<a href="' . get_field('sponsor_link') . '" target="_blank">';
        			the_post_thumbnail( 'full' );
        			if ( get_field('sponsor_link') ) echo '</a>';

        			?>
        		</div>
            <?php endwhile; ?>

        </div>
        <?php //echo $sponsors_text ?>
    </div>
</div>