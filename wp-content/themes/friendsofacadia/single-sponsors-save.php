<?php get_header(); ?>

			<div id="content" class="interior">

				<div id="inner-content" class="wrap clearfix">
					<div id="left-sidebar" class="threecol first">
						<?php

						$page_id_for_sub_nav = 4690;

						// Display the navigation for this section
						include(locate_template('section-sub-nav.php'));
						get_template_part( 'interior', 'sidebar' );

						?>
					</div>
					<div id="page-content" class="ninecol">
						<div id="mobile-section-menu">
                            Section Navigation
                        </div>
                        <div id="mobile-section-menu-items">
                        	<?php include(locate_template('section-sub-nav.php')); ?>
                        </div>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="breadcrumbs">
                                <?php if(function_exists('bcn_display')) {
                                    bcn_display();
                                } ?>
                                <div class="fb-like-wrapper"><?php echo do_shortcode( '[fb_button]' ); ?></div>
                            </div>

						    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    							<header class="article-header">

    								<div class="page-title-wrapper">
                                        <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
                                    </div>

    							</header> <!-- end article header -->

    							<section class="entry-content clearfix" itemprop="articleBody">

        							<?php
            				            $sponsor_link = get_field('sponsor_link');
            				            $sponsor_type = get_field('sponsor_type');
            				            $sponsor_address_street_line_1 = get_field('sponsor_address_street_line_1');
            				            $sponsor_address_street_line_2 = get_field('sponsor_address_street_line_2');
            				            $sponsor_address_city = get_field('sponsor_address_city');
            				            $sponsor_address_state = get_field('sponsor_address_state');
            				            $sponsor_address_zipcode = get_field('sponsor_address_zipcode');
            				            $sponsor_contact_name = get_field('sponsor_contact_name');
            				            $sponsor_contact_email = get_field('sponsor_contact_email');
            				            $sponsor_email = get_field('sponsor_email');
            				            $sponsor_phone = get_field('sponsor_phone');
            				            $sponsor_mission_statement = get_field('sponsor_mission_statement');
            				            $sponsor_centennial_plans = get_field('sponsor_centennial_plans');
            				        ?>

                                    <?php if ( has_post_thumbnail() ) : ?>

                                        <?php if (!empty($sponsor_link)) : ?>
                                        <a href="<?php echo $sponsor_link ?>" target="_blank">
                                        <?php endif; ?>
                                            <?php the_post_thumbnail( 'medium' ); ?></a>
                                        <?php if (!empty($sponsor_link)) : ?>
                                        </a>
                                        <?php endif; ?>

                                    <?php endif; ?>

                                    <?php if (!empty($sponsor_mission_statement)) : ?>
                                    <div><strong>General Description</strong></div>
                                        <p>
                                            <?php echo $sponsor_mission_statement ?>
                                        </p>
                                    <?php endif; ?>

                                    <?php if (!empty($sponsor_centennial_plans)) : ?>
                                    <div><strong>Centennial Plans</strong></div>
                                        <p>
                                            <?php echo $sponsor_centennial_plans ?>
                                        </p>
                                    <?php endif; ?>


                                    <div><strong>Contact</strong></div>
                                    <p>
                                    <?php if (!empty($sponsor_address_street_line_1)) : ?>
                                        <?php echo $sponsor_address_street_line_1 ?><br />
                                    <?php endif; ?>
                                    <?php if (!empty($sponsor_address_street_line_2)) : ?>
                                        <?php echo $sponsor_address_street_line_2 ?><br />
                                    <?php endif; ?>
                                    <?php $cityStateZip = ''; ?>
                                    <?php if (!empty($sponsor_address_city)) : ?>
                                        <?php $cityStateZip .= $sponsor_address_city ?>
                                    <?php endif; ?>
                                    <?php if (!empty($sponsor_address_state)) : ?>
                                        <?php $cityStateZip .= ', ' . $sponsor_address_state ?>
                                    <?php endif; ?>
                                    <?php if (!empty($sponsor_address_zipcode)) : ?>
                                        <?php $cityStateZip .= ' ' . $sponsor_address_zipcode ?>
                                    <?php endif; ?>

                                    <?php echo $cityStateZip ?><br />

                                    <?php if (!empty($sponsor_email)) : ?>
                                        <a href="mailto:<?php echo $sponsor_email ?>"><?php echo $sponsor_email ?></a><br />
                                    <?php endif; ?>
                                    <?php if (!empty($sponsor_phone)) : ?>
                                        <?php echo $sponsor_phone ?><br />
                                    <?php endif; ?>

                                    <?php if (!empty($sponsor_link)) : ?>
                                        <a href="<?php echo $sponsor_link ?>" target="_blank">
                                        <?php echo $sponsor_link ?>
                                        </a>
                                        </a>
                                    <?php endif; ?>
                                    </p>


                                <div class="spnsors-events">
                                <h2 class="title"> CENTENNIAL OFFERINGS </h2>

                                    <?php //echo $post->ID;

                                        $sponsor_id = $post->ID;
                                        $sponsors_events = new WP_Query();

                                            $today = date('Ymd');
                                            $args['post_type']      = 'events';
                                            $args['orderby']        = 'meta_value';
                                            $args['meta_key']        = 'event_start_date';
                                            $args['order']           = 'ASC';
                                            $args['post_status']    = 'publish';


                                            //$args['p'] = $post->ID;

                                            $args['meta_query'] = array(

                                                            array(
                                                                'key'   => 'hidden_sponsor',
                                                                'value' =>  '"'.$post->ID.'"',
                                                                'compare' => 'LIKE'
                                                            ),

                                                            array(
                                                                'key'   => 'event_start_date',
                                                                'value' => $today,
                                                                'compare' => '>='

                                                                ),

                                                            'relation'=> 'AND'

                                                        );
                                       //echo "<pre>", print_r($sponsors_events->query($args)),"</pre>";

                                        $sponsors_events->query($args);




                                    ?>
                                    <style type="text/css">
                                            .sponsors-single-events-list{
                                              padding: 5px 0;

                                            }
                                            .sponsors-single-events-list h3{
                                              font-size: 1.3em;
                                              line-height: 1.2em;
                                            }

                                            .sponsors-single-events-list span{
                                              font-size: .89em;
                                              color: #000;
                                            }
                                    </style>

                                    <?php while($sponsors_events->have_posts()): $sponsors_events->the_post(); ?>

                                        <?php $date = new DateTime(get_field('event_start_date', $post->ID)); ?>
                                    <div class="sponsors-single-events-list">
                                       <h3> <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a> <h3>
                                       <span class="date"> <?php echo $date->format('l F d, Y');  ?></span>
                                    </div>
                                    <?php endwhile; ?>

                                </div>
                                </section> <!-- end article section -->



    						</article> <!-- end article -->

						<?php endwhile; ?>

                        <?php else : ?>

								<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
									</footer>
								</article>

						<?php endif; ?>

					</div>



				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>