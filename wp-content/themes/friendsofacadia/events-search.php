<div class="foac-search-block clearfix">
	<div class="foac-events  foac-events-calender fivecol first">

								<h2 style="text-align:center">
										<?php if(isset($_GET["switch_to_month"])){
											$act_month = date_create(preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"])."-01");
											$act_month = $act_month->format('Y-m');
 										}elseif(isset($_GET["date"])){
	 										$act_month = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["date"]);
 										}else{
											$act_month = date("Y-m");
										}
										?>
										<a href="/events/?switch_to_month=<?php echo date("Y-m",strtotime($act_month." -1 month")); ?>">&laquo; Prev</a>
										<?php
											if(isset($_GET["switch_to_month"])){
												echo date("M",strtotime(preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"])."-01"));
												echo '&nbsp;' . date("Y",strtotime(preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"])."-01"));
											}else{
												echo date("F");
											}
										?>
										<a href="/events/?switch_to_month=<?php echo date("Y-m",strtotime($act_month." +1 month")); ?>" > &nbsp;Next &raquo; </a>
								</h2>
								<?php
								if(isset($_GET["switch_to_month"])){
									$new_month = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["switch_to_month"]);
									//$month_start =  $new_month."-01";
									$month_start = str_replace("-", "" , $new_month."01") ;
									//$end_of_month = $new_month."-31";//
									//$end__of_month = str_replace("-", "" , $new_month."31") ;
									$end_of_month = str_replace("-", "" , $new_month."31") ;
								}else{
									$month_start = date("Ym01");
									$today = date("Ymd");
									//$end_of_month = date("Ymt");
									//$end_of_month = str_replace("-", "" , $end_of_month);
									$end_of_month = str_replace("-", "" , date("Ymt"));
								}
								?>
								<?php

								//get all events this month
								$metaquery = array(
								 array( //checking start date
								'key' => 'event_start_date',
								'value' => $month_start,
								'compare' => '>='
								  ),
								  array( //checking until end of month
								'key' => 'event_start_date',
								'value' => $end_of_month,
								'compare' => '<='
								  ),
								);

								$update_options = array(
								'posts_per_page'   => -1,
								'orderby'          => 'meta_value',
								'meta_key'		=> 'event_start_date',
								'order'            => 'ASC',
								'meta_query'      => $metaquery,
								'post_type'        => 'events',
								'post_status'      => 'publish'
								);


								global $wp_query;
								$events = query_posts($update_options);
								//echo "<pre>", print_r($events),"</pre>";
								//echo "<pre>", print_r($update_options),"</pre>";
								//get all days with an event in an array
								$eventdays = array();
								for ($x=0;$x<count($events);$x++){
									//$date = $events[$x]->event_date;
									$date = get_field("event_start_date", $events[$x]->ID);
									array_push($eventdays, substr($date,6,2));
								}//endfor
								$today = date("d"); //today's Date

								//find 1st of Month
								$month_start = strtotime($month_start);
								$firstweekday=date('w', $month_start);
								$day = 0-$firstweekday+1;
								echo "<table>";
								//String edits for end of month calculation
								$end_of_month = str_replace("-", "" , $end_of_month);

								$month = substr($end_of_month, 4,2);
								$year = substr($end_of_month, 0,4);

								$days_in_month = cal_days_in_month(CAL_GREGORIAN,$month,$year);



								for($i=0;$i<6;$i++){
									echo "<tr>";
										for ($j=0;$j<7;$j++){
											if (!isset($_GET["switch_to_month"]) && $day == $today) echo "<td class='today' >";
											elseif (in_array($day,$eventdays)) echo "<td class='event'><a href='/events/?date=".$year."".$month."".$day."'>";
											elseif(!($day > 0 && $day <= $days_in_month)){
												echo "<td class='no-date' >";
											}else echo "<td>";

											if($day > 0 && $day <= $days_in_month) echo $day;
												$day++;
											if ($day == $today) echo "</td>";
											elseif (in_array($day,$eventdays)) echo "</a></td>";
											else echo "</td>";
										}
									echo "</tr>";
									if ($day > $days_in_month) break;
								}


								echo"</table>";
								//echo"</div>";//close calendar widget  div
								wp_reset_query(); ?>


	</div><!-- end of events calender -->

	<div class="foac-events  search sevencol last clearfix">
		<?php



		$is_search_result = false;
		$is_calendar_result = false;

		$today =  date('Ymd') ; // start date
		$plus_two_weeks = " + 14 days"; // add additional 14 days to currents day



		$default_event_search = new WP_Query(); // inatantiate WP Query by default
		$search_results = new WP_Query(); // instantiate WP Search Result

		//Setup default arguments
		$default_options = array();
		$default_options['post_type']      = 'events';
		$default_options['orderby']        = 'meta_value';
		$default_options['meta_key']		= 'event_start_date';
		$default_options['order']			= 'ASC';
		$default_options['post_status']    = 'publish';

		$default_options['meta_query'] = array(

						array(
							'key'	=> 'event_start_date',
							'value' =>  $today,
							'compare' => '>='
							//'type'	=> "NUMERIC"
						),


						array(
							'key'	=> 'event_start_date',
							'value' => date('Ymd', strtotime($today.$plus_two_weeks)),
							'compare'=> '<='
							//'type'	=> "NUMERIC"
							),

						'relation' => 'AND'


					);




		//echo "<pre>", print_r($default_event_search),"</pre>";


		if(!empty($_POST)){


			$keyword = (!empty($_POST['bykeyword'])) ? true : false;

			$search_options = array();

			if($keyword){

				$kw = trim($_POST['bykeyword'], " ");

				$foac_wp_search = $default_event_search->query('s='.trim($kw, " "));


				$default_options['post_type']      = 'events';

				$default_options['meta_query'] =
		 				array(
		 					'relation' => 'OR',
		 						array(
		 							'key' => 'event_description',
									'value' => trim($kw, " "),
		 							'compare' => 'LIKE',
		 						)
		 					);

			 		$foac_wp_custom_search = $default_event_search->query($default_options);

			 		$foac_events_search_result = array_merge($foac_wp_custom_search, $foac_wp_search);

			 		$post_ids = array();

					$is_search_result = true;

						foreach($foac_events_search_result as $res){
			 			$post_ids[] = $res->ID;

			 		}


			 			//var_dump($post_ids);

			 		//If there are post combined look in this particular posts

			 		if(count($post_ids)){
			 			$search_options['post__in'] =$post_ids;
			 		}




			} // end search by keyword


					//Fileter by
					if($_POST['bycategory'] !="all"){

						//$search_options['taxonomy'] = $_POST['bycategory'];
						//$search_options['term'] = 'events_categories';

						$search_options['tax_query'] = array(
									array(
									'taxonomy' => 'event_categories',
									'field'	=>'slug',
									'terms' => $_POST['bycategory']
									),
							);


					}




					//Search by date range

					$filter_by_start_date = !empty($_POST['start_date']) ? true : false;
					$filter_by_end_date = !empty($_POST['end_date']) ? true: false;


					//Search by the start date
					if($filter_by_start_date){

						$start_date = new DateTime($_POST['start_date']);

						//Set the end end date
						if(!$filter_by_end_date){

							$end_date = date('Ymd', strtotime($_POST['start_date'].$plus_two_weeks));

						}else{
							$set_custom_end_date = new DateTime($_POST['end_date']);

							$end_date = $set_custom_end_date->format('Ymd');
						}


						$search_options['meta_query'] = array(

							'relation' => 'AND',

								array(
								'key'	=> 'event_start_date',
								'value' => $start_date->format('Ymd'),
								'compare' => '>='
								),

								array(
								'key'	=> 'event_start_date',
								'value' => $end_date,
								'compare' => '<='
								)

							);
					}





					if(count($search_options)){
						//set the search result flag.
						$is_search_result = true;

				 			$search_options['post_type'] = 'events';
				 			$search_options['orderby']	= 'meta_value';
				 			$search_options['order_key']	= 'event_start_date';
				 			$search_options['order']		= 'ASC';
							$search_options['post_status']   = 'publish';

						//print_r($_POST);
			 			$search_results->query($search_options);


		 			}


					}


				//check to see if there is date selectes
				if(isset($_GET['date']) && !empty($_GET['date'])){

						$calendar_date = $_GET['date'];

						//get all event for this date
						$metaquery = array(
						 array( //checking start date
						'key' => 'event_start_date',
						'value' => $calendar_date,
						'compare' => '>='
						  )

						);

						$calendar_options = array(
							'posts_per_page'   => -1,
							'orderby'          => 'meta_value',
							'meta_key'		=> 'event_start_date',
							'order'            => 'ASC',
							'meta_query'      => $metaquery,
							'post_type'        => 'events',
							'post_status'      => 'publish'
						);

						$is_calendar_result = true;


					}


					//Get events for the selected month

					if(isset($_GET['switch_to_month']) && !empty($_GET['switch_to_month'])){

						$todays_date = date("d");

						$current_month = date("m");

						$calender_month = end(explode("-", $_GET['switch_to_month']));

						//Where to start the fetch from
						if($current_month===$calender_month){
							$start_fetch_at = "-".$todays_date;
						}else{
							$start_fetch_at = "-01";
						}


						$calendar_date = new DateTime($_GET['switch_to_month'].+$start_fetch_at);

						$month_calendar_date=$calendar_date->format('Ymd');

						//get all event for this date
						$metaquery = array(

							'relation' => 'AND',

							 array( //checking start date
								'key' => 'event_start_date',
								'value' => $month_calendar_date,
								'compare' => '>='
							  ),

							 array(
							 		'key' => 'event_start_date',
							 		'value' =>  date("Y".$calender_month."t"),
							 		'compare' => '<='
							 	)


						);

						$calendar_options = array(
						'posts_per_page'   => -1,
						'orderby'          => 'meta_value',
						'meta_key'		=> 'event_start_date',
						'order'            => 'ASC',
						'meta_query'      => $metaquery,
						'post_type'        => 'events',
						'post_status'      => 'publish'
						);

						$is_calendar_result = true;


					}




		?>


		<h2> Search Events </h2>
		<div class="foac-event-search clearfix">
			<a  style="display:none;" href="/our-events/" class="btn green large"> View Event Calendar </a>

			<form action="/events/" method="post">
				<div class="input-group">
					<label for="bykeyword"> Search: </label>
					<input type="text"
					<?php if(isset($_POST['bykeyword'])):?>
						value="<?php echo $_POST['bykeyword'];?>"
					<?php endif; ?>
					 id="bykeyword" name="bykeyword">
				</div>
				<div class="input-group">



					From:
						<input style="width:100px;" id="foac-event-search-start-date"

							<?php if(isset($_POST['start_date'])):?>
								value="<?php echo $_POST['start_date'];?>"
							<?php endif; ?>

						type="text" name="start_date"> To:
						<input style="width:100px;" id="foac-event-search-end-date"

							 <?php if(isset($_POST['end_date'])):?>
								value="<?php echo $_POST['end_date'];?>"
							<?php endif; ?>

						 type="text" name="end_date">

				</div>
				<div class="input-group">
					<label for="bycategory"> Category: </label>
					<select name="bycategory" id="bycategory">
						<?php $foac_events_cat_list = get_terms('event_categories'); ?>
						<option value="all">All</option>
							 <?php

					 	 foreach($foac_events_cat_list as $cat): ?>
							<?php //print_r($cat); ?>
					 	 <option value="<?php echo $cat->slug;?>"><?php echo $cat->name; ?></option>

						<?php

					 	 endforeach;
					 		?>
					</select>

				</div>
				<div class="input-group submit">
					<input type="submit" value="Search" class="btn green submit">
				</div>

			</form>
		</div> <!-- search  -->
</div><!-- / search block -->
</div>



<div class="foac-events">

<div class="event-list">

	<?php if($is_search_result): ?>


		<?php if($search_results->have_posts()): ?>

			<?php  while($search_results->have_posts()): $search_results->the_post();?>

				<div class="list-item clearfix">
				<h2 class="event-title"> <a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
				<div class="event-details">
					<div class="list-image">
						<?php
							if (has_post_thumbnail()) :
							 the_post_thumbnail( 'medium' );
				            else:
				            	//http://dummyimage.com/150x150/234F33/ffffff.png&text=FOA+Event+Image
						?>

						<img src="http://dummyimage.com/150x150/234F33/ffffff.png&text=FOA+Event+Image <?php //the_field('event_image', $post->ID); ?>" alt="<?php  the_title();?>">
						<?php endif;

							$start_date = new DateTime(get_field('event_start_date', $post->ID));
							$end_date = new DateTime(get_field('event_end_date', $post->ID));
						?>

					</div>
					<div class="details">
						<div class="date"> <?php echo $start_date->format('l F d, Y');?>

							<?php if(get_field('event_end_date')) :?>
								- <?php  echo $end_date->format('l F d, Y');?>
							<?php endif; ?>

							</div>

						<div class="text"> <?php the_field('event_description', $post->ID); ?> </div>
					</div>
				</div>
			</div><!-- // .item -->

		<?php endwhile; ?>
	<?php else:  ?>

		<div class="no-event-search results">	<h2> Sorry, there are no events that match your search. </h2> </div>



	<?php endif; else:

			if($is_calendar_result){

				$results = new WP_Query($calendar_options);

			}elseif(!$is_calendar_result){

				$results = new WP_Query($default_options);
			}


			//echo "<pre>", var_dump($calendar_options),"</pre>";
	?>

		<?php while($results->have_posts()): $results->the_post(); ?>

		<div class="list-item clearfix">
			<h2 class="event-title"> <a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<div class="event-details">
				<div class="list-image">
					<?php
						if (has_post_thumbnail()) :
						 the_post_thumbnail( 'medium' );
			            else:
			            	//http://dummyimage.com/150x150/234F33/ffffff.png&text=FOA+Event+Image
					?>

					<img src="http://dummyimage.com/150x150/234F33/ffffff.png&text=FOA+Event+Image <?php //the_field('event_image', $post->ID); ?>" alt="<?php  the_title();?>">
					<?php endif;

						$start_date = new DateTime(get_field('event_start_date', $post->ID));
						$end_date = new DateTime(get_field('event_end_date', $post->ID));
					?>

				</div>
				<div class="details">
					<div class="date"> <?php echo $start_date->format('l F d, Y');?>

					<?php if(get_field('event_end_date')): ?>
						-   <?php  echo $end_date->format('l F d, Y');?>
					<?php endif; ?>


					</div>
					<div class="text"> <?php the_field('event_description', $post->ID); ?> </div>
				</div>
			</div>
		</div><!-- // .item -->

		<?php endwhile; ?>
	<?php endif;?>


	</div>  <!-- end of events list -->

</div><!--foac events - event list -->




