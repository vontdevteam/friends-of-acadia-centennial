<?php

$menu_items = wp_get_nav_menu_items('Sidebar Navigation');
$menuItemToMatch = null;

// Get the initial menu item to start on
// global $page_id;
$menuItemToMatch = friendsofacadia_match_menu_item_for_object_id( $menu_items, $page_id_for_sub_nav );

if (!empty($menuItemToMatch)) {
	while ( empty( $menuMatchResults['parent_id'] ) ) {
		$menuMatchResults = friendsofacadia_match_menu_item_look_for_parent( $menu_items, $menuItemToMatch );
		if (!empty($menuMatchResults['menuItemToMatch'])) {
			$menuItemToMatch = $menuMatchResults['menuItemToMatch'];
		}
	}
}

if (!empty($menuMatchResults['parent_id'])) {
	$args = array(
        'menu'            => 'Sidebar Navigation',
        'submenu'         => $menuMatchResults['parent_id'],
        'container_class' => 'side-nav'
	);
	wp_nav_menu( $args );
}
else { ?>
	<div class="sidenav-spacing"><!-- --></div>
<?php } ?>