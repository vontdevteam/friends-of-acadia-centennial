<?php get_header(); ?>

			<div id="content" class="home">

				<div id="homepage-inner-content" class="wrap clearfix">
					<div id="left-sidebar" class="threecol first">
    					<?php get_template_part( 'home', 'left-sidebar' ); ?>
						<?php //get_template_part( 'common', 'left-sidebar' ); ?>
					</div>
					<div id="home-content-column" class="ninecol last">
    					<div id="home-header" class="clearfix">
    					Acadia’s Centennial: Celebrate our past! Inspire our future!
    					</div>
						<div id="home-content" class="clearfix">
							<div class="fourcol first">
								<?php get_template_part( 'home', 'event-wysiwyg' ); ?>
							</div>
							<div class="fourcol">
								
								<?php get_template_part( 'home', 'merchandise-wysiwyg' ); ?>
							</div>
							<div class="fourcol last">
								<?php get_template_part( 'home', 'partners' ); ?>
							</div>
						</div>

					</div>

				</div> <!-- end #inner-content -->

				<div class="clearfix">
				    <?php get_template_part( 'home', 'sponsors' ); ?>
				</div>

			</div> <!-- end #content -->

<?php get_footer(); ?>