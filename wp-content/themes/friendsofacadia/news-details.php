<?php

?>

<div class="news details">
		<div class="detail">

    		<header class="article-header">

				<h3 class="search-title"><?php the_title(); ?></h3>
				<p class="byline vcard"><?php
					printf(__('Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time>'), get_the_time('Y-m-j'), get_the_time(__('F jS, Y', 'bonestheme')));
				?></p>

			</header> <!-- end article header -->

            <section class="entry-content">
				<?php the_content(); ?>
            </section>

			<a href="/about-acadias-centennial/news/"> See all news  </a>

		</div>
</div>