<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once('library/bones.php'); // if you remove this, bones will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
//require_once('library/custom-post-type.php'); // you can disable this if you like
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once('library/admin.php'); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once('library/translation/translation.php'); // this comes turned off by default

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __('Sidebar 1', 'bonestheme'),
		'description' => __('The first (primary) sidebar.', 'bonestheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'bonestheme'),
		'description' => __('The second (secondary) sidebar.', 'bonestheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5($bgauthemail); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<!-- end custom gravatar call -->
				<?php printf(__('<cite class="fn">%s</cite>', 'bonestheme'), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__('F jS, Y', 'bonestheme')); ?> </a></time>
				<?php edit_comment_link(__('(Edit)', 'bonestheme'),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e('Your comment is awaiting moderation.', 'bonestheme') ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __('Search for:', 'bonestheme') . '</label>
	<input type="text" class="field" value="Search" name="s" id="s" />
	<input type="submit" class="submit" id="searchsubmit" value="'. esc_attr__('Go') .'" />
	</form>';
	return $form;

} // don't remove this bracket!

function add_first_and_last($output) {
  //$output = preg_replace('/class="menu-item/', 'class="first-menu-item menu-item', $output, 1);
  //$output = substr_replace($output, 'class="last-menu-item menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
  return $output;
}
add_filter('wp_nav_menu', 'add_first_and_last');

function friendsofacadia_display_featured_image( ) {
  global $post;

  $thumbnail_id    = get_post_thumbnail_id($post->ID);
  $thumbnail_details = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
  $thumbnail_src = wp_get_attachment_image_src( $thumbnail_id, 'full' );
  $thumbnail_width = $thumbnail_src[1];

  if ($thumbnail_src && isset($thumbnail_src[0])) {
  	echo '<div class="featured-image';
  	if ( !empty( $thumbnail_details[0]->post_excerpt ) ) echo ' wp-caption';
  	echo '" style="max-width: ' . $thumbnail_width . 'px;">';
  	if ( !empty( $thumbnail_details[0]->post_excerpt ) ) {
    	echo '<p class="featured-image-caption">';
    	echo $thumbnail_details[0]->post_excerpt;
    	echo '</p>';
    }
    the_post_thumbnail( $post->ID );
    echo '</div>';
  }
}

// Selectively load JavaScripts
add_action("wp_enqueue_scripts", "friendsofacadia_scripts_enqueue", 11);
function friendsofacadia_scripts_enqueue() {
	// Add scripts specific to the homepage
	if (is_front_page()) :
		wp_deregister_script( 'timer_script' );
		wp_register_script( 'timer_script', get_bloginfo('template_directory') . '/library/js/jquery.timer-min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'timer_script' );
		wp_deregister_script( 'homepage_script' );
		wp_register_script( 'homepage_script', get_bloginfo('template_directory') . '/library/js/home-min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'homepage_script' );
	elseif ( is_page() || is_single() ) :
		wp_deregister_script( 'page_script' );
		wp_register_script( 'page_script', get_bloginfo('template_directory') . '/library/js/page-min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'page_script' );
	endif;
	if ( is_page( 392 ) ) :
		wp_deregister_script( 'board_member_login_script' );
		wp_register_script( 'board_member_login_script', get_bloginfo('template_directory') . '/library/js/board-member-login-min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'board_member_login_script' );
	endif;
	wp_deregister_script( 'common_script' );
	wp_register_script( 'common_script', get_bloginfo('template_directory') . '/library/js/common-min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'common_script' );
}

// Get rid of the paragraph and [...] from excerpt
remove_filter('the_excerpt', 'wpautop');

// Set a custom length for the article excerpts
function custom_excerpt_length( $length ) {
	return 21;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_filter( 'wp_nav_menu_objects', 'friendsofacadia_submenu_limit', 10, 2 );

/**
 * friendsofacadia_submenu_limit function
 * Display part of a WordPress navigation menu by filtering out items
 * @param array $items
 * @param array $args
 * @return array
 */
function friendsofacadia_submenu_limit( $items, $args ) {

    if ( empty($args->submenu) ) {
        return $items;
    }

    $parent_id = $args->submenu;
    $elements_to_display  = friendsofacadia_submenu_get_children_ids( $parent_id, $items );

    // Add the parent to the array
    array_unshift($elements_to_display, $parent_id);

    foreach ( $items as $key => $item ) {
        if ( ! in_array( $item->ID, $elements_to_display ) ) {
            unset($items[$key]);
        }
    }

    return $items;
}

/**
 * friendsofacadia_submenu_get_children_ids function
 * Get the child menu items for a WordPress menu item
 * @param int $id
 * @param array $items
 * @return array
 */
function friendsofacadia_submenu_get_children_ids( $id, $items ) {

	$ids = wp_filter_object_list( $items, array( 'menu_item_parent' => $id ), 'and', 'ID' );

    foreach ( $ids as $id ) {
        $ids = array_merge( $ids, friendsofacadia_submenu_get_children_ids( $id, $items ) );
    }

    return $ids;
}

/**
 * friendsofacadia_match_menu_item_for_object_id
 * @param array $menuItems
 * @param int $object_id
 * @return id
 */
function friendsofacadia_match_menu_item_for_object_id( $menuItems, $object_id ) {
	$menu_item_id = null;

	foreach( $menuItems as $itemKey => $itemValue ) {
		if ( $object_id == $menuItems[$itemKey]->object_id ) {
			$menu_item_id = $menuItems[$itemKey]->ID;
		}
	}
	return $menu_item_id;
}

/**
 * friendsofacadia_match_menu_item_look_for_parent
 * Get the parent of the passed WordPress menu item id
 * Return the next parent to look for, if the parent has a parent
 * @param array $menuItems
 * @param int $menuItemToMatch
 * @return array
 */
function friendsofacadia_match_menu_item_look_for_parent( $menuItems, $menuItemToMatch ) {
	$results = array(
		'menuItemToMatch' => null,
		'parent_id'       => null
	);
	foreach( $menuItems as $itemKey => $itemValue ) :
		if ( $menuItemToMatch == $menuItems[$itemKey]->ID ) :
			// We have a match
			if ($menuItems[$itemKey]->menu_item_parent == 0) {
				$results['parent_id'] = $menuItems[$itemKey]->ID;
			}
			else {
				$results['menuItemToMatch'] = $menuItems[$itemKey]->menu_item_parent;
			}
		endif;
	endforeach;
	return $results;
}

function friendsofacadia_how_you_can_help( $atts, $content = null ) {
	return '<div class="how-you-can-help">' . $content . '</div>';
}
add_shortcode( 'how_you_can_help', 'friendsofacadia_how_you_can_help' );

function friendsofacadia_how_you_can_help_half_size( $atts, $content = null ) {
	return '<div class="how-you-can-help_half_size">' . $content . '</div>';
}
add_shortcode( 'how_you_can_help_half_size', 'friendsofacadia_how_you_can_help_half_size' );

function friendsofacadia_myfeed_request( $qv ) {
    if (isset($qv['feed']) && !isset($qv['post_type']))
        $qv['post_type'] = array('post', 'news', 'events');
    return $qv;
}
add_filter('request', 'friendsofacadia_myfeed_request');

function dump($thing, $label=null) {

	if (!DEBUG_MODE) {
		return;
	}

	echo '<xmp style="margin-top:90px;border:2px dashed #999; background:yellow; font-family:Consolas,monospace; font-size:12px; padding:10px;">';
	echo 'Dump' . iif(!is_null($label), ' of ' . $label) . ":\n";
	print_r($thing);
	echo '</xmp>';

}

function iif($condition, $true, $false=null) {
	return ($condition) ? $true : $false;
}


$result = add_role("sponsor","Sponsor/Partner", $cap=null);

$role = get_role( 'sponsor' );
//$role->add_cap( 'edit_posts' );

// Added to extend allowed files types in Media upload
add_filter('upload_mimes', 'custom_upload_mimes');

function custom_upload_mimes ( $existing_mimes=array() ) {
    // Add *.EPS files to Media upload
    $existing_mimes['eps'] = 'application/postscript';
    // Add *.AI files to Media upload
    $existing_mimes['ai'] = 'application/postscript';

    return $existing_mimes;
}

/* custom login logo */
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/library/images/logo-centennial.png);

        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
function my_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	global $user;
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {

    	// check for sponsors
    	if ( in_array( 'sponsor', $user->roles ) ) {
            return get_permalink(PAGE_ID_SPONSOR_HOME);
        }

		// check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} else {
			return home_url();
		}

	} else {
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

add_action('get_header', 'process_post');

/*
    Limit protected pages by role
*/

function process_post() {

    global $post;

    $id = $post->ID;

    $sponsors_or_admins_only = get_field('sponsors_or_admins_only', $id);

    if ($sponsors_or_admins_only == 1) :

        $user = wp_get_current_user();
        if ( isset( $user->roles ) && is_array( $user->roles ) ) :
            // check for sponsors or admins
        	if ((in_array('sponsor', $user->roles)) or (in_array('administrator', $user->roles))) :
                // Ok, stay where we are
            else :
                auth_redirect();
            endif;
        else:
            auth_redirect();
        endif;
    endif;
}

add_filter('gform_field_value_hidden_sponsor_name', 'set_sponsor_name');



function set_sponsor_name($value) {

    $user = wp_get_current_user();

    $sponsor_field = get_field('sponsor', 'user_' . $user->ID);
    if (is_array($sponsor_field)) {
        $sponsor_id = $sponsor_field[0];
    }

    //$sponsor = get_post ($sponsor_id);

    $sponsor_name = get_the_title($sponsor_id);

    return $sponsor_name;

}

/*** ADD YYYYMMDD Format to event start date and end date which are not supported by GForm by default 05/28/15 SH***/

add_filter( 'gform_save_field_value', 'save_event_start_date', 10, 4);

function save_event_start_date($value, $lead, $field, $form){

	if($form['id']== 4 && $field['id']==4){
		$formatted = new DateTime($value);
		return $formatted->format('Ymd');
	}else{
		return $value;
	}
}


add_filter( 'gform_save_field_value', 'save_event_end_date', 10, 4);

function save_event_end_date($value, $lead, $field, $form){

	if($form['id']== 4 && $field['id']==5){
		$formatted = new DateTime($value);
		return $formatted->format('Ymd');
	}else{
		return $value;
	}
}








add_filter('gform_field_value_hidden_sponsor', 'set_sponsor_id');
function set_sponsor_id($value) {

    $user = wp_get_current_user();

    $sponsor_field = get_field('sponsor', 'user_' . $user->ID);
    if (is_array($sponsor_field)) {
        $sponsor_id = $sponsor_field[0];
    }

    return $sponsor_id;

}
