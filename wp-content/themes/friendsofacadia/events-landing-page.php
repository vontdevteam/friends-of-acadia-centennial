<?php
/*
Template Name: Events landing page
*/
?>

<?php get_header(); ?>

			<div id="content" class="interior">

				<div id="inner-content" class="wrap clearfix">
					<div id="left-sidebar" class="threecol first">
						<?php

						$page_id_for_sub_nav = $post->ID;

						// Display the navigation for this section
						include(locate_template('section-sub-nav.php'));
						get_template_part( 'interior', 'sidebar' );

						?>
					</div>
					<div id="main" class="ninecol last" role="main">
						<div id="mobile-section-menu">
                            Section Navigation
                        </div>
                        <div id="mobile-section-menu-items">
                        	<?php include(locate_template('section-sub-nav.php')); ?>
                        </div>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="breadcrumbs">
                                <?php if(function_exists('bcn_display')) {
                                    bcn_display();
                                } ?>
                                <div class="fb-like-wrapper"><?php echo do_shortcode( '[fb_button]' ); ?></div>
                            </div>

						    <article id="post-<?php the_ID(); ?>" <?php post_class( array( 'clearfix', 'archive-page' )); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    							<header class="article-header">

    								<div class="page-title-wrapper">
                                        <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
                                    </div>

    							</header> <!-- end article header -->

    							<section class="entry-content clearfix" itemprop="articleBody">
                                    <?php if ( has_post_thumbnail() ) :
                                        friendsofacadia_display_featured_image();
                                    endif; ?>
    								<?php the_content(); ?>

    								<?php get_template_part('events','search'); ?>

                                </section> <!-- end article section -->

    						</article> <!-- end article -->

    						<?php
        						/*
    						// Get a list of blog posts
	                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	                        $blog_query = new WP_Query();

							$args['post_type'] = 'events';
							$args['meta_key']  = 'start_date';
							$args['orderby']   = 'start_date';
							$args['order']     = 'ASC';
							$args['showposts'] = -1;
							$args['paged']     = $paged;

	                        $blog_query->query( $args );

	                        ?><table border="0" width="100%" cellpadding="8" cellspacing="4"><?php

	                        // The Loop
	                        while ( $blog_query->have_posts() ) : $blog_query->the_post();

	                        	get_template_part( 'event', 'listing-item' );

	                        endwhile; ?>
	                    	</table><?php

	                        $previous_link = get_previous_posts_link('Previous');
	                        $next_link = get_next_posts_link('Next', $blog_query->max_num_pages);
	                        ?>
	                        <div class="blog-pagination-wrapper"><?php
	                            if ( !empty( $next_link ) ) { ?>
	                                <div class="next-link"><?php echo $next_link; ?></div>
	                            <?php }
	                            if ( !empty( $previous_link ) ) { ?>
	                                <div class="previous-link"><?php echo $previous_link; ?></div>
	                            <?php } ?>
	                        </div><?php

	                        if (function_exists('bones_page_navi')) { ?>
	                        		<?php bones_page_navi(); ?>
	                        <?php } else { ?>
	                        		<nav class="wp-prev-next">
	                        				<ul class="clearfix">
	                        					<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
	                        					<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
	                        				</ul>
	                        		</nav>
	                        <?php }
                            */
	                    ?>
						<?php endwhile; ?>
                        <?php else : ?>

								<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
									</footer>
								</article>

						<?php endif; ?>

						</div> <!-- end #main -->

					</div>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>