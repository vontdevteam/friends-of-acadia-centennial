<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php
        if ( is_home() ) :
            echo get_bloginfo();
        else :
            wp_title('');
        endif;
        ?></title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- drop Google Analytics Here -->

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-36626577-2', 'auto');
          ga('send', 'pageview');

        </script>

		<!-- end analytics -->

		<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/css3-mediaqueries.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/ie8.css">
		<![endif]-->

        <!--<script type="text/javascript" src="http://fast.fonts.com/jsapi/cb04ba6a-7690-45b4-8508-7777a5d3d550.js"></script>-->
        <script type="text/javascript" src="http://fast.fonts.net/jsapi/739a299e-2740-4297-b935-3de52000e12d.js"></script>

        <script type="text/javascript">
            jQuery(window).load(function() {
                jQuery('#mobile-menu').bind('click touch', function() {
                    jQuery('#menu-primary-menu').toggleClass('display-nav');
                });
                jQuery('#mobile-section-menu').bind('click touch', function() {
                    jQuery('#mobile-section-menu-items').toggleClass('display-nav');
                });
                jQuery('#quick-links-menu').bind('click touch', function() {
                    jQuery('#quick-links-menu-items').toggleClass('display-nav');
                });
            });
        </script>

	</head>

	<body <?php body_class(); ?>>

		<div id="container">
            <?php
            // Display the header, which includes the homepage slideshow, on the homepage
            if (is_home()) : ?>
    			<header class="home header" role="banner">

                    <div class="search-form-wrapper">
                        <div class="search-form">
                            <?php get_search_form( true ); ?>
                        </div>
                    </div>

                    <?php
                    // Get the homepage slideshow
                    get_template_part( 'content', 'home-slideshow' );

                    ?>
					<nav class="home" role="navigation">
                        <div class="primary-nav-wrapper">
                            <a href="/"><div class="logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/logo-centennial.png" alt="<?php bloginfo('name'); ?>" /></div></a>
                            <div id="mini-donate-button" class="donate-button"><a href="<?php the_field('donation_form', 'option'); ?>">Donate</a></div>
                            <div id="mobile-header-clear" class="clearfix"><!-- --></div>
                            <div id="quick-links-menu">
                                Quick Links
                            </div>
                            <div id="quick-links-menu-items">
                                <?php get_template_part( 'quick', 'links' ); ?>
                            </div>
                            <div id="mobile-menu">
                                Main Navigation
                            </div>
                            <?php bones_main_nav(); ?>
                        </div>
					</nav>
    			</header> <!-- end header -->
            <?php
            else : ?>
                <header class="header interior" role="banner">
                    <div class="search-form-wrapper">
                        <div class="search-form">
                            <?php get_search_form( true ); ?>
                        </div>
                    </div>

                    <div id="page-banner">
                        <?php get_template_part( 'page', 'header' ); ?>
                    </div>

					<nav class="home" role="navigation">
                        <div class="primary-nav-wrapper">
                        	<div class="logo"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/library/images/logo-centennial.png" alt="<?php bloginfo('name'); ?>" /></a></div>
                            <div id="mini-donate-button" class="donate-button"><a href="<?php the_field('donation_form', 'option'); ?>">Donate</a></div>
                            <div id="mobile-header-clear" class="clearfix"><!-- --></div>
                            <div id="quick-links-menu">
                                Quick Links
                            </div>
                            <div id="quick-links-menu-items">
                                <?php get_template_part( 'quick', 'links' ); ?>
                            </div>
                            <div id="mobile-menu">
                                Main Navigation
                            </div>
                            <?php bones_main_nav(); ?>
                        </div>
					</nav>

                </header>
            <?php endif;