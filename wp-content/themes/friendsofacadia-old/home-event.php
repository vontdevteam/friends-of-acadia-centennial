<?php
$events_block_query = new WP_Query();

$today = date('Y-m-d');

$args = array();
$args['post_type']      = 'events';
$args['meta_key']       = 'start_date';
$args['orderby']        = 'start_date';
$args['order']          = 'DESC';
$args['post_status']    = 'publish';
$args['meta_query'][] = array(
	'key'       => 'display_on_homepage',
	'value'     => true,
	'compare'   => '='
);
$args['posts_per_page'] = 3;

$events_block_query->query($args);

?>

<section class="article-listing">
	<header class="home-section-title"><h2>Calendar</h2></header>
	<?php
	$i = 1; // Counter to show photo
	while ($events_block_query->have_posts() ) : $events_block_query->the_post(); ?>
		<article>
			<?php if ( $i == 1 ) : ?>
				<div class="event-poster">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
				</div>
			<?php endif; ?>
			<header><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3></header>
			<section>
				<?php the_excerpt(); ?>
			</section>
		</article>
	<?php $i = $i + 1;
	endwhile; ?>
	<div class="archive-link">
		<a href="/get-involved/events/">see all events
		<img src="<?php echo get_template_directory_uri(); ?>/library/images/archive-arrow.png" alt="see all events" /></a>
	</div>
</section>