// Set a value for our timer - here is where we place what to do
var timer = jQuery.timer(function() {
	showNextSlide();
});
timer.set({ time : 8000, autostart : true });

function positionBeAFriendBox() {
	if ( (!jQuery.browser.msie || jQuery.browser.version >= 8.0) ) {
		var contentHeight = jQuery('#home-content-column').outerHeight();
		contentHeight = contentHeight - 26;

		//jQuery('#left-sidebar').height(contentHeight);
	}

}

function displaySlide(slideToShow, previousSlideNumber) {
	jQuery.slideshowVariables.slideInTransition = 1;
	// Fade out the current slide
	jQuery('#slide' + previousSlideNumber).add('.caption').fadeOut(1000, function() {
		jQuery('#slideshow-caption' + previousSlideNumber).css('display', 'none');
	});

	// Fade in the next slide
	jQuery('#slide' + slideToShow).add('.caption').fadeIn(2000, function() {
		jQuery('#slideshow-caption' + slideToShow).css('display', 'block');
		// Restart the time to move to the next one
		jQuery.slideshowVariables.slideInTransition = 0;
	});

}

/* Display the next slide */
function showNextSlide() {
	if (jQuery.slideshowVariables.slideInTransition !== 1) {
		// If we are at the end of the list, we want to move back to the first slide
		var previousSlideNumber = jQuery.slideshowVariables.currentSlide;
		if (jQuery.slideshowVariables.currentSlide === jQuery.slideshowVariables.numberOfSlides) {
			jQuery.slideshowVariables.currentSlide = 1;
		}
		else { // We still have more slides, so simply increment to the next one
			jQuery.slideshowVariables.currentSlide++;
		}

		displaySlide(jQuery.slideshowVariables.currentSlide, previousSlideNumber);
	}
}

/* Display the previous slide */
function showPreviousSlide() {
	if (jQuery.slideshowVariables.slideInTransition !== 1) {

		// If we are at the beginning of the list, we want to move back to the last slide
		var previousSlideNumber = jQuery.slideshowVariables.currentSlide;
		if (jQuery.slideshowVariables.currentSlide === 1) {
			jQuery.slideshowVariables.currentSlide = jQuery.slideshowVariables.numberOfSlides;
		}
		else { // We still have previous slides, so simply increment to subtract one
			jQuery.slideshowVariables.currentSlide--;
		}

		displaySlide(jQuery.slideshowVariables.currentSlide, previousSlideNumber);
	}
}

jQuery(document).ready(function() {
	jQuery(window).load(function() {
		//positionBeAFriendBox();
	});
	jQuery(window).resize(function() {
		//positionBeAFriendBox();
	});

	// Set up a global container so we can use the values throughout the script
	jQuery.slideshowVariables = new Object({});
	// Get the number of slides that we have using jQuery
	jQuery.slideshowVariables.numberOfSlides = jQuery('#home-slideshow .slide').length;
	// Set a value to use to keep track of what number slide we are on
	jQuery.slideshowVariables.currentSlide = 1;
	// Set a flag for the slide being in transition - we want to disable the slideshow navigation while the slides are changing
	jQuery.slideshowVariables.slideInTransition = 0;

	/*
	jQuery('.caption-text .text').each(function() {
		var self = jQuery(this);
		self.html( self.text().replace(/\s(\S+)$/,"&nbsp;$1") );
	});
	*/

	// Bind the next and previous slideshow arrows
	jQuery('#previous-slide').click(function() {
		timer.stop();
		showPreviousSlide();
		timer.play();
	});
	jQuery('#next-slide').click(function() {
		timer.stop();
		showNextSlide();
		timer.play();
	});

});