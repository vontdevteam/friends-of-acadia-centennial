<?php
$header_query = new WP_Query();

$args['post_type']      = 'page_headers';
$args['orderby']        = 'rand';
$args['post_status']    = 'publish';
$args['posts_per_page'] = 1; // Limit the number of slides to 10

$header_query->query($args);

// Run through the returned headers
while ($header_query->have_posts() ) : $header_query->the_post();

	// Get the id of the featured (header) image
	the_post_thumbnail('full');

endwhile;

wp_reset_query();

?>