							<?php
    							$sponsor_blurb = get_field('sponsor_blurb');
    							$sponsor_centennial_plans = get_field('sponsor_centennial_plans');
    							if (empty($sponsor_blurb)) :
    							    $sponsor_blurb = wp_trim_words($sponsor_centennial_plans, 50);
    							endif;
    				        ?>
							<article id="post-<?php the_ID(); ?>" <?php post_class( array('clearfix', 'sponsors-listing') ); ?> role="article">

								<header class="article-header">

									<h3 class="search-title"><?php the_title(); ?></h3>


								</header> <!-- end article header -->

								<section class="entry-content">

									<?php
									    $fields = get_fields();
									    //dump($fields);

									    $file_object = get_field('image_file');
									    $attachment_id = $file_object['id'];

                                        $url = wp_get_attachment_url( $attachment_id );
                                        $title = get_the_title();


                                        if (wp_attachment_is_image($attachment_id)) :

                                            $thumbnail_image_array = wp_get_attachment_image_src( $attachment_id, 'thumb' );
                                            $thumbnail_image = $thumbnail_image_array[0];

                                        else:

                                            $thumbnail_image_object = get_field('image_thumbnail');
                                            $thumbnail_image = $thumbnail_image_object['sizes']['medium'];

                                        endif;

								    ?>

								    <div><a href="<?php echo $url ?>"><img src="<?php echo $thumbnail_image ?>" /></a></div>

								    <div><?php the_content(); ?></div>

                                    <div><a href="<?php echo $url ?>">Download image file</a></div>

								</section> <!-- end article section -->

								<footer class="article-footer">

								</footer> <!-- end article footer -->

							</article> <!-- end article -->