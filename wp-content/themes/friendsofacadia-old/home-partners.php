<?php

// get random sponsor

$sponsors_block_query = new WP_Query();

$args = array();
$args['post_type']      = 'sponsors';
$args['orderby']        = 'rand';
$args['post_status']    = 'publish';
$args['posts_per_page'] = 1;

$sponsors_block_query->query( $args );

$right_column_text = get_field('right_column_text', 'option');

?>

<section class="article-listing">
	<header class="home-section-title"><h2>Partners</h2></header>

	<p><?php echo $right_column_text ?></p>

    <?php while ($sponsors_block_query->have_posts() ) : $sponsors_block_query->the_post(); ?>
		<article class="partner">
    		<!--<a href="<?php echo get_permalink() ?>"><?php echo get_the_title() ?></a>-->

			<?php
            // link to sponsor single
			echo '<a href="' . get_permalink() . '">';

			if (has_post_thumbnail()) :
			    the_post_thumbnail( 'medium' );
            endif;

            echo '</a>';

			if (get_field('sponsor_blurb')) :
			    echo '<div class="blurb">' . get_field('sponsor_blurb') . '</div>';
			endif;

			echo '<a class="arrow" href="' . get_permalink() . '">';
            echo 'Learn more';
            echo '</a>';

			?>
			<div class="clearfix"></div>
		</article>
    <?php endwhile; ?>
	<div class="archive-link">
		<a class="arrow" href="/centennial-partners">More Acadia Centennial Partners
		<!--<img src="<?php echo get_template_directory_uri(); ?>/library/images/archive-arrow.png" alt="see all partners" />--></a>
	</div>
</section>