<?php
/*
Template Name: Newsroom template
*/
?>

<?php get_header(); ?>

			<div id="content" class="interior">

				<div id="inner-content" class="wrap clearfix">
					<div id="left-sidebar" class="threecol first">
						<?php

						$page_id_for_sub_nav = $post->ID;

						// Display the navigation for this section
						include(locate_template('section-sub-nav.php'));
						get_template_part( 'interior', 'sidebar' );

						?>
					</div>
					<div id="page-content" class="ninecol last archive-list-container" role="main">
						<div id="mobile-section-menu">
                            Section Navigation
                        </div>
                        <div id="mobile-section-menu-items">
                        	<?php include(locate_template('section-sub-nav.php')); ?>
                        </div>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="breadcrumbs">
                                <?php if(function_exists('bcn_display')) {
                                    bcn_display();
                                } ?>
                                <div class="fb-like-wrapper"><?php echo do_shortcode( '[fb_button]' ); ?></div>
                            </div>

						    <article id="post-<?php the_ID(); ?>" <?php post_class( array( 'clearfix', 'archive-page' )); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    							<header class="article-header">

    								<div class="page-title-wrapper">
                                        <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
                                    </div>

    							</header> <!-- end article header -->

    							<section class="entry-content clearfix" itemprop="articleBody">
                                    <?php if ( has_post_thumbnail() ) :
                                        friendsofacadia_display_featured_image();
                                    endif; ?>
    								<?php the_content(); ?>

                                </section> <!-- end article section -->

    						</article> <!-- end article -->

    						<div class="newsroom-category">
	    						<h2>In the News</h2>
	    						<?php
	    						// Get a list of In the News stories
		                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		                        $blog_query = new WP_Query();

		                        $args['post_type'] = 'news';
		                        $args['orderby']   = 'date';
		                        $args['order']     = 'DESC';
		                        $args['showposts'] = 5;
		                        $args['paged']     = $paged;
		                        $args['tax_query'] = array(
		                                    array(
                                                'taxonomy' => 'news_categories',
                                                'field'    => 'slug',
                                                'terms'    => 'in-the-news'
                                            ),
                                );

		                        $blog_query->query( $args );

		                        // The Loop
		                        while ( $blog_query->have_posts() ) : $blog_query->the_post();
		                        	get_template_part( 'archive', 'listing-item' );
		                        endwhile;

		                        wp_reset_query();

		                        ?>
		                        <div class="archive-link">
		                        	<a href="<?php echo get_permalink(4907); ?>">View all In the News stories.</a>
		                        </div>
		                    </div>

		                   	<div class="newsroom-category">
		                        <h2>Press Releases</h2>
		                        <?php
								// Get a list of featured stories
		                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		                        $blog_query = new WP_Query();

		                        $args['post_type'] = 'news';
		                        $args['orderby']   = 'date';
		                        $args['order']     = 'DESC';
		                        $args['showposts'] = 5;
		                        $args['paged']     = $paged;
		                        $args['tax_query'] = array(
            		                        array(
                                                'taxonomy' => 'news_categories',
                                                'field'    => 'slug',
                                                'terms'    => 'press-releases'
                                            ),
                                );

		                        $blog_query->query( $args );

		                        // The Loop
		                        while ( $blog_query->have_posts() ) : $blog_query->the_post();
		                        	get_template_part( 'archive', 'listing-item' );
		                        endwhile;

		                        wp_reset_query();

		                        ?>
		                        <div class="archive-link">
		                        	<a href="<?php echo get_permalink(4741); ?>">View all Press Releases.</a>
		                        </div>
		                    </div>
		                    <div class="padding-for-bottom"><!-- --></div>

						<?php endwhile; ?>
                        <?php else : ?>

								<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
									</footer>
								</article>

						<?php endif; ?>
						<?php //get_template_part( 'll-bean', 'logo' ); ?>
						<?php //get_template_part( 'mobile', 'enews-signup' ); ?>

						</div> <!-- end #main -->

					</div>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>