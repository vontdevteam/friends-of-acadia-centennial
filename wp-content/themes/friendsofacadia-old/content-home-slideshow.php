<?php

$slides = array();

$slideshow_query = new WP_Query();

$args['post_type']      = 'homepage_slides';
$args['orderby']        = 'menu_order';
$args['order']          = 'ASC';
$args['post_status']    = 'publish';
$args['posts_per_page'] = 10; // Limit the number of slides to 10

$slideshow_query->query($args);

// Run through the returned slides
$i = 1; // Set up a counter for the slides
while ($slideshow_query->have_posts() ) : $slideshow_query->the_post();

	// Get the id of the featured (slideshow) image
	$featuredImageID = get_post_thumbnail_id($post->ID);
    $slides[$i]['slideshowPhoto'] = wp_get_attachment_image($featuredImageID, 'full');
    $slides[$i]['caption'] = get_the_content();
    $slides[$i]['slideshowLink'] = get_field('slideshow_link');
    $slides[$i]['slideshowText'] = get_field('slideshow_link_text');

    $i = $i + 1;
endwhile;

?>

<div id="home-slideshow-wrapper">
	<div id="home-slideshow">
		<?php foreach($slides as $slideKey => $slideValue) : ?>
			<div class="slide" id="slide<?php echo $slideKey; ?>"><?php echo $slides[$slideKey]['slideshowPhoto']; ?></div>
		<?php endforeach; ?>
	</div>
</div>

<div class="caption-wrapper">
	<div class="caption-box">
		<div class="caption">
			<div class="caption-text">
				<?php foreach($slides as $slideKey => $slideValue) : ?>
					<div id="slideshow-caption<?php echo $slideKey; ?>" class="text"><?php echo $slides[$slideKey]['caption']; ?></div>

				<?php endforeach; ?>
			</div>
			<div class="home-slideshow-nav">
				<div id="next-slide" class="right"><img src="<?php echo get_template_directory_uri(); ?>/library/images/caption-nav-right.png" alt="Next Slide" /></div>
				<div id="previous-slide" class="left"><img src="<?php echo get_template_directory_uri(); ?>/library/images/caption-nav-left.png" alt="Previous Slide" /></div>
				<?php
				foreach($slides as $slideKey => $slideValue) :
					if (!empty($slides[$slideKey]['slideshowLink'])) :
						echo '<div id="slideLink' . $slideKey . '" class="slide-link">';
						echo '<a href="' . $slides[$slideKey]['slideshowLink'] . '">';
						if (!empty($slides[$slideKey]['slideshowText'])) :
							echo $slides[$slideKey]['slideshowText'];
						endif;
						echo ' &raquo;</a>';
						echo '</div>';
					endif;
				endforeach; ?>
			</div>
		</div>
	</div>
</div>
