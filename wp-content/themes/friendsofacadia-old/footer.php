			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">
					<div class="clearfix">
						<div class="sixcol first footer-contact">
                            This website is a project of the
							<h5>Acadia Centennial Task Force</h5>
							<div class="address">
    							c/o Friends of Acadia<br />
								43 Cottage Street, P.O. Box 45, Bar Harbor, Maine  04609<br />
								(207) 288-3340 toll-free: (800) 625-0321
							</div>
							<div class="links">
    							<div>
        							<a target="_blank" href="http://www.friendsofacadia.org">www.friendsofacadia.org</a>&nbsp;&nbsp;|&nbsp;&nbsp;
								<a target="_blank" href="http://www.nps.gov/acad">www.nps.gov/acad</a>
								</div>

								<div>
								<a target="_blank" href="http://friendsofacadia.org/about-us/contact-us/">Contact Friends of Acadia</a>&nbsp;&nbsp;|&nbsp;&nbsp;
								<a target="_blank" href="http://www.nps.gov/acad/contacts.htm ">Contact Acadia National Park</a>
								</div>
							</div>
						</div>
						<div class="threecol">
                            <?php foa_footer_links_left(); ?>
						</div>
						<div class="threecol last">
                            <?php foa_footer_links_right(); ?>
						</div>
					</div>
					<div class="wrap clearfix copyright">
						&copy; <?php echo date('Y'); ?> Copyright Acadia Centennial Task Force
					</div>
				</div> <!-- end #inner-footer -->


			</footer> <!-- end footer -->

		</div> <!-- end #container -->

		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

	</body>

</html>
